package com.devconcrete.benchmark;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.devconcrete.benchmark.model.Usuario;
import com.devconcrete.benchmark.service.UsuarioService;



@RunWith(SpringRunner.class)
@WebMvcTest
public class UsuarioTests {

  Logger logger = Logger.getLogger(UsuarioTests.class);

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private UsuarioService usuarioService;

  @Test
  public void testObterUsuario() throws Exception {

    // Cria o mock do usuário
    Usuario mockUsuario = new Usuario("João da Silva", 25, "Brasilia");

    // Define o comportamento do obterUsuario
    when(usuarioService.obterUsuario(any(String.class))).thenReturn(mockUsuario);

    // Executa e valida o status e conteudo do JSON
    MvcResult resultado = mockMvc
        .perform(get("/usuarios/{id}", "1").accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(content().json("{'nome':'João da Silva','idade':25, 'cidade':'Brasilia'}"))
        .andReturn();

    logger.info(resultado.getResponse().getContentAsString());
  }
}
