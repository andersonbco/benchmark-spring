package com.devconcrete.benchmark.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.devconcrete.benchmark.model.Usuario;

public interface UsuarioRepository extends MongoRepository<Usuario, String> {

  Usuario findById(String id);

  List<Usuario> findByIdade(int idade);
}
