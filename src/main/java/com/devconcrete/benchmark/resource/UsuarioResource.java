package com.devconcrete.benchmark.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devconcrete.benchmark.model.Usuario;
import com.devconcrete.benchmark.service.UsuarioService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/usuarios")
public class UsuarioResource {

  @Autowired
  private UsuarioService usuarioService;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<Usuario>> listarUsuarios() {
    return ResponseEntity.status(HttpStatus.OK).body(usuarioService.listarUsuarios());
  }

  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Usuario> obterUsuario(@PathVariable("id") String id) {
    return ResponseEntity.status(HttpStatus.OK).body(usuarioService.obterUsuario(id));
  }

  @GetMapping(value = "/idade/{idade}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<Usuario>> listarUsuariosPorIdade(@PathVariable("idade") int idade) {
    return ResponseEntity.status(HttpStatus.OK).body(usuarioService.listarUsuariosPorIdade(idade));
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Usuario> criarUsuario(@RequestBody Usuario usuario) {
    return ResponseEntity.status(HttpStatus.CREATED).body(usuarioService.criarUsuario(usuario));
  }

  @DeleteMapping("{/id}")
  public ResponseEntity<Usuario> excluirUsuario(@PathVariable("id") String id) {
    usuarioService.excluirUsuario(id);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }
}
