package com.devconcrete.benchmark.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devconcrete.benchmark.model.Usuario;
import com.devconcrete.benchmark.repository.UsuarioRepository;

@Service
public class UsuarioService {

  @Autowired
  private UsuarioRepository usuarioRepository;

  public List<Usuario> listarUsuarios() {
    return usuarioRepository.findAll();
  }

  public Usuario obterUsuario(String id) {
    return usuarioRepository.findById(id);
  }

  public List<Usuario> listarUsuariosPorIdade(int idade) {
    return usuarioRepository.findAll().parallelStream().filter(p -> p.getIdade() == idade)
        .collect(Collectors.toList());
  }

  public Usuario criarUsuario(Usuario usuario) {

    usuarioRepository.save(usuario);

    return usuario;
  }

  public void excluirUsuario(String id) {
    usuarioRepository.delete(id);
  }
}
